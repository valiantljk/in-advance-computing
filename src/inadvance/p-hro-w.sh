#!/bin/bash
#$ -V
#$ -cwd
#$ -S /bin/bash
#$ -N prodata-write
#$ -o prodata-write-$JOB_ID.o
#$ -e prodata-write-$JOB_ID.e
#$ -q normal
#$ -pe fill 12
#$ -P hrothgar


cmd="mpirun -np 12 ./prodata-write -f /lustre/scratch/jialliu/data/syn-12.nc  -c 120 -l 100 -t 100 -n 10"
$cmd
