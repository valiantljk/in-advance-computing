#include <libmemcached/memcached.h>
#include <mpi.h>
#ifndef iomodel_H_INCLUDE
#define iomodel_H_INCLUDE
struct Layout{
    int ndim;	//num of dimension
    int * dim;	//length 
    int nproc;	//num of process
    int nagg;	//num of aggregator
    int aggbu;	
    int strip;	//stripe size in MB
    int count;
    int raidLev;//level of RAID, default 6, 8+2
    int diskbuf;//disk buffer size
};

bool ReuseOrnot(struct Layout * ,int * , int * );

#endif
