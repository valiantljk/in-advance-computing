#include <libmemcached/memcached.h>
#ifndef READDB_H_INCLUDE
#define READDB_H_INCLUDE
#define RTYPE float
typedef struct {
RTYPE * subset;
int totalquery;
int newstart;
}Reuse;


Reuse readdb(memcached_st * memc, memcached_return * rc, char * op, int * new_analysis_range,int chunk_size);

#endif
