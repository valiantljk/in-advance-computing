#include <stdio.h>
#include <stdlib>
#include <mpi.h>
#include <pnetcdf.h>
#include "iomodel"

/*
 *Started July 7 2015 at Parkview, Lubbock, TX
 *In progress.......
 *
 */


struct Layout{
//dataset info
    int ndim;	//num of dimension
    int * dim;	//length of each dim
//process info
    int nproc;	//num of process
    int nagg;	//num of aggregator
    int aggbu;	//buffer size of aggregator
//Lustre info
    int strip;	//stripe size in MB
    int count;	//stripe count, i.e., num of OST
//RAID info
    int raidLev;//level of RAID, default 6, 8+2
    int diskbuf;//disk buffer size
};


//given i/o (start, length), make decesion on 'reuse the result or not'

bool ReuseOrnot(struct Layout * ly,int * start, int * length ){

/*predict I/O performance

.......ongoing............

*/
return true;//default
}
